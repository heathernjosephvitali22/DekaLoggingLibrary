/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "stdoutlogtarget.hpp"
#include <iostream>

namespace DekaLoggingLibrary {

void StdOutLogTarget::output(LogMessagePtr msg)
{
    if (!msg)
        return;

    if (formatter) {
        formatter->setMsg(msg);
        std::cout << formatter->getMessage() << std::endl;
    } else {
        std::cout << msg->getMessage() << std::endl;
    }
    std::cout.flush();
}

void StdOutLogTarget::setFormatter(LogMessageFormatterPtr newformatter)
{
    formatter = newformatter;
}

String StdOutLogTarget::getName() const
{
    return "Standart StdOut log target";
}

}
