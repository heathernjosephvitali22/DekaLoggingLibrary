/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "logmessageinfo.hpp"

namespace DekaLoggingLibrary {

LogMessageInfo::LogMessageInfo()
    : priority("FATAL", 0)
    , category("")
{
}

LogCategory LogMessageInfo::getCategory() const
{
    return category;
}

void LogMessageInfo::setCategory(const LogCategory& value)
{
    category = value;
}

LogPriority LogMessageInfo::getPriority() const
{
    return priority;
}

void LogMessageInfo::setPriority(const LogPriority& value)
{
    priority = value;
}

}
