/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "logmessage.hpp"
#include <stdexcept>

namespace DekaLoggingLibrary {

LogMessage::LogMessage()
    : logfinished(false)
    , buffer()
    , info()
{
}

LogMessage::~LogMessage()
{
    if (!logfinished) {
        //TODO log msg
    }
}

bool LogMessage::isFinished() const
{
    //auto guard = std::lock_guard(mutex);
    return logfinished;
}

void LogMessage::waitUntilFinished()
{
    auto lock = std::unique_lock(mutex);
    auto condition = [this]() -> bool { return isFinished(); };
    condVar.wait(lock, condition);
}

LogMessage& LogMessage::operator<<(short value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(unsigned short value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(int value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(unsigned int value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(long value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(unsigned long value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(long long value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(unsigned long long value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(float value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(double value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(long double value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(bool value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(const void* value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(std::nullptr_t value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(Char value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(String value)
{
    if (!logfinished)
        buffer << value;
    return *this;
}

LogMessage& LogMessage::operator<<(std::ios_base& (*func)(std::ios_base&))
{
    if (!logfinished)
        buffer << func;
    return *this;
}

LogMessage& LogMessage::operator<<(std::basic_ios<Char>& (*func)(std::basic_ios<Char>&))
{
    if (!logfinished)
        buffer << func;
    return *this;
}

LogMessage& LogMessage::operator<<(std::basic_ostream<Char>& (*func)(std::basic_ostream<Char>&))
{
    if (!logfinished)
        buffer << func;
    return *this;
}

LogMessage& LogMessage::operator<<(const LogPriority& priority)
{
    if (!logfinished)
        info.setPriority(priority);
    return *this;
}

LogMessage& LogMessage::operator<<(const LogCategory& category)
{
    if (!logfinished)
        info.setCategory(category);
    return *this;
}

LogMessage& LogMessage::operator<<(LogEndMark)
{
    auto guard = std::lock_guard(mutex);
    logfinished = true;
    condVar.notify_all();
    return *this;
}

String LogMessage::getMessage() const
{
    return buffer.str();
}

LogMessageInfo LogMessage::getMessageInfo() const
{
    return info;
}

}
