/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "logcategory.hpp"
#include "logpriority.hpp"
#include "typedefs.hpp"

namespace DekaLoggingLibrary {

class DEKALOGGINGLIBRARY_EXPORT LogMessageInfo {
private:
    LogPriority priority;
    LogCategory category;

public:
    LogMessageInfo();
    ~LogMessageInfo() = default;

    LogPriority getPriority() const;
    void setPriority(const LogPriority& value);

    LogCategory getCategory() const;
    void setCategory(const LogCategory& value);
};

}
