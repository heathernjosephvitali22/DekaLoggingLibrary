/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "log.hpp"
#include <cassert>
#include <chrono>
#include <future>

namespace DekaLoggingLibrary {

Log* Log::instance = nullptr;

Log::Log()
    : thread(nullptr)
    , running(false)
{
    thread = std::make_shared<std::thread>([this] { mainLoop(); });
}

Log::~Log()
{
    running = false;
    thread->join();
    assert(empty());
}

LogMessage& Log::newMessage()
{
    LogMessagePtr msg = std::make_shared<LogMessage>();
    push(msg);
    return *msg; // msg will not deleted on function exit. It is in queue
}

void Log::addTarget(ILogTargetPtr target)
{
    targets.push_back(target);
}

void Log::mainLoop()
{
    while (running) {
        pollMessageAndTranslate();

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    //Pulling all messages
    while (!empty()) {
        pollMessageAndTranslate();
    }
}

void Log::pollMessageAndTranslate()
{

    LogMessagePtr msgPtr = top();

    if (!msgPtr)
        return;

    assert(msgPtr);
    msgPtr->waitUntilFinished();
    translateMsgToTargets(msgPtr);
    pop();
}

void Log::translateMsgToTargets(LogMessagePtr msg)
{
    std::list<std::future<void>> listFutures;
    for (auto trgt : targets) {
        auto newFuture = std::async(std::launch::async, [=]() { trgt->output(msg); });
        listFutures.push_back(std::move(newFuture));
    }

    for (auto& future : listFutures) {
        future.wait();
    }
}

void Log::push(LogMessagePtr ptr)
{
    auto lock = std::lock_guard(mutexQueue);
    queue.push(std::move(ptr));
    condVarQueue.notify_one();
}

LogMessagePtr Log::top()
{
    auto lock = std::unique_lock(mutexQueue);
    auto condition = [this] { return !empty(); };

    bool cond = condVarQueue.wait_for(lock, std::chrono::milliseconds(100), condition);
    if (cond) {
        return queue.front();
    }
    return nullptr;
}

void Log::pop()
{
    auto lock = std::unique_lock(mutexQueue);
    auto condition = [this] { return !empty(); };

    bool cond = condVarQueue.wait_for(lock, std::chrono::milliseconds(100), condition);
    if (cond) {
        queue.pop();
    }
}

bool Log::empty()
{
    //auto lock = std::lock_guard(mutexQueue);
    return queue.empty();
}

void Log::InitGlobal()
{
    if (!instance) {
        instance = new Log();
    }
}

Log& Log::getInstance()
{
    return *instance;
}

void Log::DestroyGlobal()
{
    if (instance) {
        delete instance;
        instance = nullptr;
    }
}

}
