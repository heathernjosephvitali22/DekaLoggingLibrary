/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "ilogmessage.hpp"
#include "logcategory.hpp"
#include "logmessageinfo.hpp"
#include "logpriority.hpp"
#include <condition_variable>
#include <iostream>
#include <memory>
#include <mutex>

namespace DekaLoggingLibrary {

class LogMessage;
using LogMessagePtr = std::shared_ptr<LogMessage>;

class DEKALOGGINGLIBRARY_EXPORT LogEndMark {
};

class DEKALOGGINGLIBRARY_EXPORT LogMessage : public ILogMessage {
private:
    bool logfinished;
    StringStream buffer;

    LogMessageInfo info;

    mutable std::mutex mutex;
    mutable std::condition_variable condVar;

public:
    LogMessage();
    ~LogMessage() override;

    bool isFinished() const;
    void waitUntilFinished();

    LogMessage& operator<<(short value);
    LogMessage& operator<<(unsigned short value);

    LogMessage& operator<<(int value);
    LogMessage& operator<<(unsigned int value);

    LogMessage& operator<<(long value);
    LogMessage& operator<<(unsigned long value);

    LogMessage& operator<<(long long value);
    LogMessage& operator<<(unsigned long long value);

    LogMessage& operator<<(float value);
    LogMessage& operator<<(double value);
    LogMessage& operator<<(long double value);

    LogMessage& operator<<(bool value);
    LogMessage& operator<<(const void* value);
    LogMessage& operator<<(std::nullptr_t);

    LogMessage& operator<<(Char value);
    LogMessage& operator<<(String value);

    LogMessage& operator<<(std::ios_base& (*func)(std::ios_base&));
    LogMessage& operator<<(std::basic_ios<Char>& (*func)(std::basic_ios<Char>&));
    LogMessage& operator<<(std::basic_ostream<Char>& (*func)(std::basic_ostream<Char>&));

    LogMessage& operator<<(const LogPriority& priority);
    LogMessage& operator<<(const LogCategory& category);
    LogMessage& operator<<(LogEndMark);

    // ILogMessage interface
public:
    String getMessage() const override;
    LogMessageInfo getMessageInfo() const override;
};

}
