/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "dekalogginglibrary_export.hpp"
#include "ilogtarget.hpp"
#include "logmessage.hpp"
#include <condition_variable>
#include <list>
#include <mutex>
#include <queue>
#include <thread>

namespace DekaLoggingLibrary {

class DEKALOGGINGLIBRARY_EXPORT Log {
private:
    std::list<ILogTargetPtr> targets;

    std::queue<LogMessagePtr> queue;
    std::mutex mutexQueue;
    std::condition_variable condVarQueue;

    std::shared_ptr<std::thread> thread;

    bool running;

public:
    Log();
    ~Log();

    LogMessage& newMessage();
    void addTarget(ILogTargetPtr target);

private:
    void mainLoop();
    void pollMessageAndTranslate();
    void translateMsgToTargets(LogMessagePtr msg);

    //Queue operations
    void push(LogMessagePtr ptr);
    LogMessagePtr top();
    void pop();
    bool empty();

    //Singleton
private:
    static Log* instance;

public:
    static void InitGlobal();
    static Log& getInstance();
    static void DestroyGlobal();
};

}
