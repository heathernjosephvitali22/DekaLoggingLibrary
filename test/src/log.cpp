/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "log.hpp"
#include <chrono>
#include <future>
#include <gtest/gtest.h>
#include <thread>

using namespace DekaLoggingLibrary;
using namespace testing;

class TestLogTarget : public ILogTarget {
private:
    String lastMsg = "";

    // ILogTarget interface
public:
    void output(LogMessagePtr msg) override { lastMsg = msg->getMessage(); }
    void setFormatter(LogMessageFormatterPtr formatter) override { (void)formatter; }
    String getName() const override { return "Test Target"; }

    String getLastMsg() const { return lastMsg; }
};

class TestLog : public Test {
protected:
    std::shared_ptr<Log> log;
    std::shared_ptr<TestLogTarget> target;
    // Test interface
protected:
    void SetUp() override
    {
        log = std::make_shared<Log>();
        target = std::make_shared<TestLogTarget>();
        log->addTarget(target);
    }
    void TearDown() override
    {
        log = nullptr;
        target = nullptr;
    }
};

TEST_F(TestLog, MessagesDelivers)
{
    std::string message = "test";

    auto lastMsg = target->getLastMsg();
    ASSERT_EQ(lastMsg, "");

    log->newMessage() << message << LogEndMark();

    int timeOut = 5000;
    for (int i = 0; i < timeOut; i++) {
        if (target->getLastMsg() == message)
            break;
        std::this_thread::sleep_for(std::chrono::milliseconds(1)); // Give some time
    }

    ASSERT_EQ(target->getLastMsg(), message);
}
