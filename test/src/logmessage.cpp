/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "logmessage.hpp"
#include <chrono>
#include <future>
#include <gtest/gtest.h>
#include <thread>

using namespace DekaLoggingLibrary;
using namespace testing;

class TestLogMessage : public Test {
protected:
    LogMessagePtr msgPtr;

    // Test interface
protected:
    void SetUp() override
    {
        msgPtr = std::make_shared<LogMessage>();
    }
    void TearDown() override
    {
        msgPtr = nullptr;
    }
};

TEST_F(TestLogMessage, EndMark)
{
    ASSERT_FALSE(msgPtr->isFinished());

    *msgPtr << LogEndMark();

    ASSERT_TRUE(msgPtr->isFinished());
}

TEST_F(TestLogMessage, waitUntilFinished)
{
    ASSERT_FALSE(msgPtr->isFinished());

    auto future = std::async(std::launch::async, [=] {
        msgPtr->waitUntilFinished();
    });

    *msgPtr << LogEndMark();
    ASSERT_NE(future.wait_for(std::chrono::milliseconds(5000)), std::future_status::timeout);

    ASSERT_TRUE(msgPtr->isFinished());
}

TEST_F(TestLogMessage, LogPriorityChange)
{
    LogPriority newPriority("TEST", -100);

    ASSERT_NE(msgPtr->getMessageInfo().getPriority(), newPriority);

    *msgPtr << newPriority;

    ASSERT_EQ(msgPtr->getMessageInfo().getPriority().getName(), newPriority.getName());
    ASSERT_EQ(msgPtr->getMessageInfo().getPriority().getPriority(), newPriority.getPriority());
}

TEST_F(TestLogMessage, LogCategoryChange)
{
    LogCategory newCategory("TEST");

    ASSERT_NE(msgPtr->getMessageInfo().getCategory(), newCategory);

    *msgPtr << newCategory;

    ASSERT_EQ(msgPtr->getMessageInfo().getCategory(), newCategory);
}
