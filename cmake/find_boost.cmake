macro(find_boost)

    find_package(Boost 1.72.0 REQUIRED COMPONENTS filesystem)

endmacro(find_boost)
