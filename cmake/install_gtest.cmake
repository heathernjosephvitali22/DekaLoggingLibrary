macro(install_gtest)
    include(FetchContent)
    FetchContent_Declare(
        googletest
        GIT_REPOSITORY      https://github.com/google/googletest.git
        GIT_TAG             release-1.10.0
        PREFIX              ${CMAKE_CURRENT_BINARY_DIR}/external/gtest
    )

    FetchContent_GetProperties(googletest)
    if(NOT googletest_POPULATED)

        FetchContent_Populate(googletest)
        set(GOOGLETEST_FORCE_SHARED_CRT ON CACHE BOOL "Use shared crt for gtest instead static")

        if(GOOGLETEST_FORCE_SHARED_CRT)
            set(gtest_force_shared_crt ON CACHE BOOL "")
        else()
            set(gtest_force_shared_crt OFF CACHE BOOL "")
        endif()

        set(INSTALL_GTEST OFF CACHE BOOL "")
        mark_as_advanced(INSTALL_GTEST)

        add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
        
        set_target_properties(gtest PROPERTIES FOLDER extern)
        set_target_properties(gtest_main PROPERTIES FOLDER extern)
        set_target_properties(gmock PROPERTIES FOLDER extern)
        set_target_properties(gmock_main PROPERTIES FOLDER extern)
    endif()
endmacro(install_gtest)
